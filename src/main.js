import Vue from 'vue'
import './plugins/vuetify'
import App from './App.vue'

Vue.config.productionTip = false

/*
  Icons
*/
import { library } from '@fortawesome/fontawesome-svg-core'
import { faAt, faSchool, faUniversity, faBriefcase, faWindowClose, faBars } from '@fortawesome/free-solid-svg-icons'
import { faGithub, faGit, faLinkedin } from '@fortawesome/free-brands-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'
library.add(faGithub, faGit, faAt, faLinkedin, faSchool, faUniversity, faBriefcase, faWindowClose, faBars);
Vue.component('font-awesome-icon', FontAwesomeIcon)

new Vue({
  render: h => h(App),
}).$mount('#app')
