import Vue from 'vue'
import Vuetify, {
  VApp, // required
  VToolbar,
  VParallax,
  VTimeline,
  VNavigationDrawer,
} from 'vuetify/lib'
import 'vuetify/src/stylus/app.styl'

Vue.use(Vuetify, {
  components: {
    VApp,
    VParallax,
    VTimeline,
    VToolbar,
    VNavigationDrawer
  },
  iconfont: 'fa',
})
